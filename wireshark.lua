apex_proto = Proto("apex","The Apex Network")

function apex_proto.dissector(buffer, pinfo, tree)
    pinfo.cols.protocol = "APEX"
    local subtree = tree:add(apex_proto, buffer(), "Apex Header")

    subtree:add(buffer(0, 1), "version "  ..  buffer(0, 1):uint())
    subtree:add(buffer(1, 3), "reserved " ..  buffer(1, 3))
    subtree:add(buffer(4, 4), "route "    ..  buffer(4, 4):uint64())
    subtree:add(buffer(8, 8), "counter "  ..  buffer(8, 8):uint64())
end
-- load the tcp.port table
tcp_table = DissectorTable.get("tcp.port")
-- register our protocol to handle tcp port 7200
tcp_table:add(7200, apex_proto)
