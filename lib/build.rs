extern crate prost_build;

fn main() {
    prost_build::compile_protos(&[
            "src/proto/apex/core.proto",
            "src/proto/apex/acquire.proto",
            "src/proto/apex/configure.proto",
        ],
        &["src/"]).unwrap();
}
