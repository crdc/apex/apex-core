#[derive(Serialize, Deserialize, Debug)]
pub struct Message {
    pub id: String,
    //pub msg_type: Option<MessageType>,
}

impl Message {
    pub fn new() -> Message {
        Self {
            id: "msg".to_string(),
            //msg_type: None,
        }
    }
}

// struct or enum for req/rep message types?

#[derive(Serialize, Deserialize, Debug)]
pub enum MessageType {
    REQ,
    REP,
    PUB,
    SUB,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct RequestMessage {
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ReplyMessage {
}

#[derive(Serialize, Deserialize, Debug)]
pub struct PublishMessage {
}

#[derive(Serialize, Deserialize, Debug)]
pub struct SubscribeMessage {
}

// pub enum Broadcast ?

pub enum Request {
}

pub enum Reply {
}

pub enum Publish {
    CONFIG,
    LOG,
    METRIC,
    MEASUREMENT,
    HEARTBEAT,
    EXPERIMENT,
}

/// Message module
pub mod analyze {

    // XXX: should these be PluginRequest { Add, List, ... } ?

    pub enum Request {
        ReadStatus,
        OpenChannel,
        CloseChannel,
        ListChannels,
        AddPlugin,
        RemovePlugin,
        ListPlugins,
        EnablePlugin,
        DisablePlugin,
        ReloadPlugin,
    }

    pub enum Reply {
        Status,
        ChannelOpened,
        ChannelClosed,
        ChannelList,
        PluginAdded,
        PluginRemoved,
        PluginList,
        PluginEnabled,
        PluginDisabled,
        PluginReloaded,
    }
}

pub mod configure {

}
