extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
extern crate prost;
#[macro_use]
extern crate prost_derive;
extern crate prost_types;

pub mod macros;
pub mod endpoint;
pub mod message;
pub mod object;
pub mod property;
pub mod channel;
pub mod plugin;

pub mod prelude {
    //! The `apex` prelude
    //!
    //! //! The purpose of this module is to alleviate imports of many common apex
    //! traits by adding a glob import to the top of apex heavy modules:
    //!
    //! ```
    //! # #![allow(unused_imports)]
    //! use apex::prelude::*;
    //! ```

    pub use message::{
        Message,
    };

    pub use apex::core::v1::{
        Object,
        Property,
        Channel,
        Plugin,
    };
}

mod apex {
    pub use super::*;

    // Protobuf includes
    // TODO: make v1::{core,configure,acquire}
    pub mod core {
        pub mod v1 {
            include!(concat!(env!("OUT_DIR"), "/apex.core.v1.rs"));
        }
    }

    pub mod acquire {
        pub mod v1 {
            include!(concat!(env!("OUT_DIR"), "/apex.acquire.v1.rs"));
        }
    }

    pub mod configure {
        pub mod v1 {
            include!(concat!(env!("OUT_DIR"), "/apex.configure.v1.rs"));
        }
    }
}
