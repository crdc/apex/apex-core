use std::io::Cursor;

use prost::Message;

use apex::core::v1::Channel;

impl Channel {
    pub fn new() -> Channel {
        Channel::default()
    }

    pub fn serialize(&self) -> Vec<u8> {
        let mut buf = Vec::new();
        buf.reserve(self.encoded_len());
        self.encode(&mut buf).unwrap();
        buf
    }

    pub fn deserialize(&self, buf: &[u8]) -> Result<Channel, prost::DecodeError> {
        Channel::decode(&mut Cursor::new(buf))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn can_serialize() {
        let mut channel = Channel::new();
        let bytes = vec![
            10, 9, 97, 112, 101, 120, 45, 116, 101, 115, 116,
            16, 144, 78, 24, 232, 7
        ];

        channel.owner = "apex-test".to_string();
        channel.port = 10000;
        channel.subscriber_count = 1000;

        let buf = channel.serialize();

        assert_eq!(buf, bytes);
    }

    #[test]
    fn can_deserialize() {
        let channel = Channel::new();
        let bytes = vec![
            10, 9, 97, 112, 101, 120, 45, 116, 101, 115, 116,
            16, 144, 78, 24, 232, 7
        ];

        let msg = channel.deserialize(&bytes)
            .expect("Failed to deserialize channel");

        assert_eq!(msg.owner, "apex-test");
        assert_eq!(msg.port, 10000);
        assert_eq!(msg.subscriber_count, 1000);
    }
}
