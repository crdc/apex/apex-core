use std::io::Cursor;

use prost::Message;

use apex::core::v1::Property;

impl Property {
    pub fn new() -> Property {
        Property::default()
    }

    pub fn serialize(&self) -> Vec<u8> {
        let mut buf = Vec::new();
        buf.reserve(self.encoded_len());
        self.encode(&mut buf).unwrap();
        buf
    }

    pub fn deserialize(&self, buf: &[u8]) -> Result<Property, prost::DecodeError> {
        Property::decode(&mut Cursor::new(buf))
    }
}

#[cfg(test)]
mod tests {
    //use super::*;

    #[test]
    fn can_serialize() {
        //let mut property = Property::new();
        //let bytes = vec![
        //];

        //property. = "".to_string();

        //let buf = property.serialize();

        //assert_eq!(buf, bytes);
    }

    #[test]
    fn can_deserialize() {
        //let mut property = Property::new();
        //let bytes = vec![
        //];

        //let msg = property.deserialize(&bytes)
            //.expect("Failed to deserialize property");

        //assert_eq!(msg., "");
    }
}
