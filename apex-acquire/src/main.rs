//! Apex acquisition service
//!
//! This application is used to ingest data from a variety of sources and
//! provide it over ZMQ sockets, most commonly REQ/REP and PUB/SUB.

extern crate apex;

#[macro_use]
extern crate log;
extern crate env_logger;
extern crate clap;
extern crate config;
extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
extern crate zmq;
extern crate futures;
extern crate tokio;
extern crate tokio_zmq;

use std::env;
use std::error::Error;
use std::process;
use std::sync::Arc;

use clap::{App};
use futures::{Future, Stream};
use tokio_zmq::{prelude::*, Rep};

use apex::prelude::*;

//mod server;
mod settings;

//use server::Server;
use settings::Settings;

/// Network services examples:
/// https://stevedonovan.github.io/rust-gentle-intro/7-shared-and-networking.html
/// - lots of useful stuff there, eg. ping host, execute shell command
/// http://siciarz.net/24-days-of-rust-zeromq/
/// - simple example showing looping req/rep echo server
/// https://learning-0mq-with-pyzmq.readthedocs.io/en/latest/pyzmq/patterns/patterns.html
/// - ZMQ patterns
///
/// XXX: possibly use a router/dealer setup
/// XXX: this example could be relevant
/// https://github.com/erickt/rust-zmq/blob/master/examples/msgsend/main.rs
///
/// TODO: review req/rep patterns from ch. 3/4 of zquide
/// TODO: change to log4rs/fern for log handler
/// TODO: switch to REQ-ROUTER or DEALER-ROUTER, apparently REQ-REP is wrong due to some things not always needing a REP

fn main() {
    if let Err(_) = env::var("RUST_LOG") {
        env::set_var("RUST_LOG", "apex=info,apex-acquire");
    }
    env_logger::init();

    match run() {
        Ok(_) => {}
        Err(e) => {
            error!("{}", e);
            process::exit(1);
        }
    }
}

/// Server should do these things:
///
/// 0. load configuration
/// 0. construct threadpool (rayon?)
/// 1. receive requests to open a new pub/sub channel
///    - pub is acquire server side
///    - sub is record/control/analysis side
/// 2. receive request for a list of pub/sub channels
/// 3. receive requests to open a new sub/pub channel
///    - sub is acquire server side
///    - pub is outproc plugin functioning as data producer
fn run() -> Result<(), Box<Error>> {

    let matches = App::new("apex-acquire")
        .version("0.1.1")   // TODO: get from Cargo.toml
        .author("Apex authors")
        .about("Apex acquisition service")
        .args_from_usage("-c, --config=[FILE] 'Sets a config file'
                         -v... 'Enable verbose logging'")
        .get_matches();

    //let local: String;
    if let Some(c) = matches.value_of("config") {
        info!("Loading configuration from {}", c);
        //local = c;
    }

    //let settings = Settings::new(local);
    let settings = Settings::new(None);
    match settings {
        Ok(_) => {
            info!("{:?}", settings);
        }
        Err(e) => {
            // TODO: gracefully warn and exit on invalid configuration
            panic!("{}", e);
        }
    }

    // TODO: figure out how to use this instead of RUST_LOG (log4rs/fern)
    match matches.occurrences_of("v") {
        0 => info!("normal"),
        1 => info!("verbose"),
        2 => info!("very verbose"),
        3 | _ => info!("very very verbose"),
    }

    // TODO: on new publish channel request spawn thread
    // TODO: for each client thread create an mpsc channel

    let context = Arc::new(zmq::Context::new());
    let service = settings.unwrap().service.acquire;
    let endpoint = format!("tcp://{}:{}", service.bind, service.port);
    debug!("Listening on endpoint {}", endpoint);

    let sock = Rep::builder(context).bind(endpoint.as_str()).build();

    // XXX: not sure if this is a broadcast, or if all endpoints
    // connect to a service that accepts these
    inform("apex-acquire".to_string())?;

    /*
     *let mut msg = zmq::Message::new().unwrap();
     *loop {
     *    match sock.recv(&mut msg, 0) {
     *        Ok(_) => {
     *            info!("Received {}", msg.as_str().unwrap());
     *            let resp = handle_message(&msg)?;
     *            sock.send(resp, 0)?;
     *        }
     *        Err(e) => {
     *            error!("{}", e);
     *        }
     *    }
     *}
     */

    let runner = sock.and_then(|rep| {
        // XXX: no idea what the 25 is for rn (multipart size?)
        let (sink, stream) = rep.sink_stream(25).split();

        stream
            .map(|multipart| {
                for msg in &multipart {
                    if let Some(s) = msg.as_str() {
                        info!("Received: {}", s);
                    }
                }
                // TODO: handle the message here
                // eg. handle_message(multipart).unwrap()
                // or endpoint.handle(multipart) -> SinkItem
                multipart
            })
            .forward(sink)
    });

    tokio::run(runner.map(|_| ()).or_else(|e| {
        error!("Error: {}", e);
        Ok(())
    }));

    Ok(())
}

fn inform(name: String) -> Result<(), Box<Error>> {
    // Broadcast notify message to inform network
    // TODO: get ID from config
    let message = Message {
        id: name,
        //msg_type: Some(apex::message::MessageType::REQ),
    };
    info!("{}", serde_json::to_string_pretty(&message)?);

    Ok(())
}

/// TODO: put this in a server struct that enforces `server.handle(msg) -> msg`
fn _handle_message(msg: &zmq::Message) -> Result<&zmq::Message, Box<Error>> {
    let deser: Message = serde_json::from_str(msg.as_str().unwrap())?;
    info!("{:?}", serde_json::to_string_pretty(&deser)?);

    // TODO: construct response message
    // TODO: return response
    // TODO: change & on return and don't return reference to existing message

    Ok(msg)
}
