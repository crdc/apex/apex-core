use std::error::Error;

use settings::Settings;

// XXX: this is probably better as a reusable Endpoint

pub struct Server {
    settings: Option<Settings>,
    sock: zmq::Socket,
}

impl Server {
    pub fn new(settings: Settings) -> Result<Server, Box<Error>> {
        let context = zmq::Context::new();
        let sock = context.socket(zmq::REP).unwrap();

        let service = settings.service.acquire;
        let endpoint = format!("tcp://{}:{}", service.bind, service.port);
        debug!("Listening on endpoint {}", endpoint);

        sock.bind(endpoint.as_str())
            .expect("Couldn't start service on port");

        Ok(Self {
            settings: None,
            sock,
        })
    }

    pub fn inform() -> Result<(), Box<Error>> {
        Ok(())
    }

    pub fn handle() -> Result<(), Box<Error>> {
        Ok(())
    }
}
