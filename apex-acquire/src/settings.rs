use config::{ConfigError, Config, File, Environment};
use std::env;
//use std::error::Error;

#[derive(Debug, Serialize, Deserialize)]
struct Database {
    host: String,
    port: i32,
    username: String,
    password: Option<String>,
    database: String,
}

mod service {
    #[derive(Debug, Serialize, Deserialize)]
    pub struct Service {
        pub config: Config,
        pub acquire: Acquire,
    }

    #[derive(Debug, Serialize, Deserialize)]
    pub struct Config {
        pub bind: String,
        pub port: i32,
    }

    #[derive(Debug, Serialize, Deserialize)]
    pub struct Acquire {
        pub bind: String,
        pub port: i32,
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Settings {
    debug: bool,
    database: Database,
    pub service: service::Service,
}

impl Settings {
    pub fn new(local: Option<String>) -> Result<Self, ConfigError> {
        let mut s = Config::new();

        // TODO: make this OS independent with std::path::Path
        let conf_dir = env::var("CONF_DIR").unwrap_or("/etc/apex".into());

        // Merge defaults in first
        s.merge(File::with_name(&format!("{}/defaults", conf_dir)).required(true))?;

        // Add in the current environment file, defaulting to 'development'
        let env = env::var("RUN_MODE").unwrap_or("development".into());
        s.merge(File::with_name(&format!("{}/{}", conf_dir, env)).required(false))?;

        // Add in the local user config
        if let Some(f) = local {
            s.merge(File::with_name(&format!("{}", f)).required(false))?;
        }

        // Add settings from the environment (with a prefix of APEX)
        // eg. `APEX_DEBUG=1 ./target/apex` sets the `debug` key
        s.merge(Environment::with_prefix("apex"))?;

        // Deserialize the entire config freezing it
        s.try_into()
    }

/*
 *    pub fn get<T>(&self, key: &str) -> Result<T, Box<Error>>
 *    where T: serde::de::DeserializeOwned
 *    {
 *        let s = Config::try_from(&self).unwrap();
 *        let value = s.get::<T>(key).unwrap();
 *
 *        Ok(value)
 *    }
 */
}
